import QtQuick 2.5

Rectangle {
    property alias mouseArea: mouseArea

    width: 360
    height: 360

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    Image {
        id: basicMap
        objectName: "basicMap"
        width: 10
        height: 286
        clip: false
        source: "images/basicmap.png"
        fillMode: Image.Tile
        asynchronous: true
        anchors.centerIn: parent
    }
}
