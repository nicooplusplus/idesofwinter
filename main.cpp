#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QDebug>
#include <QQmlProperty>
#include <QImage>
#include <QColor>
#include <QList>
#include <QPoint>
#include <QRgb>
#include <QTest>
#include <QPainter>
#include <QVector>
#include <QString>

enum class Direction
{
    NORTH,
    EAST,
    SOUTH,
    WEST
};

QRgb sea = QColor(174,201,254).rgb();
QRgb land = QColor(2,186,3).rgb();

struct Edge
{
    QPoint start;
    Direction direction;
    int length;
    Edge(QPoint pt, Direction dir): start(pt), direction(dir), length(1){};
    QString dir(){

        switch(direction)
        {
        case Direction::EAST:
            return "EAST";
        case Direction::NORTH:
            return "NORTH";
        case Direction::SOUTH:
            return "SOUTH";
        case Direction::WEST:
            return "WEST";
        }


    }


};

QList<Edge*> edgesList = QList<Edge*>();
QList<QPoint> markerMap = QList<QPoint>();
QVector<QPoint> controlPoints = QVector<QPoint>();
typedef QVector<QPoint> River;
QList<River> riversList = QList<River>();

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine aengine;
    aengine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    QQmlEngine engine;
    QQmlComponent component(&engine,QUrl(QStringLiteral("qrc:/MainForm.ui.qml")));
    QObject *object = component.create();

    QObject *image = object->findChild<QObject*>("basicMap");
    if(image)
    {
        QQmlProperty(image, "fillMode").write("Image.Stretch");

        QQmlProperty(image, "width").write(500);
    }

    QImage* qimage = new QImage(":/images/basicmap.png");

    if ( qimage->isNull())
        qCritical() << "Error image not loaded ";

    bool markerFounded = false;
    QPoint cP(0,0); // current position
    Edge* cE; // current edge

    // get the first marker
    for(int j=0 ; j<qimage->height() ; j++)
        for(int i=0 ; i<qimage->width() ; i++)
        {
            if( qimage->pixel(QPoint(i,j)) ==  land )
            {
                markerFounded = true;
                Edge* edge = new Edge(QPoint(i,j),Direction::EAST);
                edgesList.append(edge); // does it really needed here ?
                cE = edge;
                cP = QPoint(i,j);
                markerMap.append(cP);
            }

            if ( markerFounded )
                break;
        }

    // get edges list
    bool finish = false;
    while(not finish)
    {
        switch( cE->direction )
        {
        case Direction::NORTH:
        {
            QPoint pNW(cP.x()-1,cP.y()-1);
            QPoint pN(cP.x(),cP.y()-1);
            if ( markerMap[0] == pNW || markerMap[0] == pN )
                finish = true;
            if ( qimage->pixel(pNW) ==  land )
            {
                Edge* edge = new Edge(cP,Direction::WEST);
                                cP = pNW;
                edgesList.append(edge);
                cE = edge;
            }
            else if ( qimage->pixel(pN) ==  sea )
            {
                Edge* edge = new Edge(cP,Direction::EAST);
                edgesList.append(edge);
                cE = edge;
            }
            else // pE is land
            {
                cE->length++;
                cP = pN;
            }
            break;
        }
        case Direction::EAST:
        {
            QPoint pNE(cP.x()+1,cP.y()-1);
            QPoint pE(cP.x()+1,cP.y());
            if ( markerMap[0] == pNE || markerMap[0] == pE )
                finish = true;
            if ( qimage->pixel(pNE) ==  land )
            {
//                cP = pNE;
                Edge* edge = new Edge(cP,Direction::NORTH);
                                cP = pNE;
                edgesList.append(edge);
                cE = edge;
            }
            else if ( qimage->pixel(pE) ==  sea )
            {
                Edge* edge = new Edge(cP,Direction::SOUTH);
                edgesList.append(edge);
                cE = edge;
            }
            else // pE is land
            {
                cE->length++;
                cP = pE;
            }
            break;
        }
        case Direction::SOUTH:
        {
            QPoint pSE(cP.x()+1,cP.y()+1);
            QPoint pS(cP.x(),cP.y()+1);
            if ( markerMap[0] == pSE || markerMap[0] == pS )
                finish = true;
            if ( qimage->pixel(pSE) ==  land )
            {
//                cP = pSE;
                Edge* edge = new Edge(cP,Direction::EAST);
                                cP = pSE;
                edgesList.append(edge);
                cE = edge;
            }
            else if ( qimage->pixel(pS) ==  sea )
            {
                Edge* edge = new Edge(cP,Direction::WEST);
                edgesList.append(edge);
                cE = edge;
            }
            else // pS is land
            {
                cE->length++;
                cP = pS;
            }
            break;
        }
        case Direction::WEST:
        {
            QPoint pSW(cP.x()-1,cP.y()+1);
            QPoint pW(cP.x()-1,cP.y());
            if ( markerMap[0] == pSW || markerMap[0] == pW )
                finish = true;
            if ( qimage->pixel(pSW) ==  land )
            {
//                cP = pSW;
                Edge* edge = new Edge(cP,Direction::SOUTH);
                                cP = pSW;
                edgesList.append(edge);
                cE = edge;
            }
            else if ( qimage->pixel(pW) ==  sea )
            {
                Edge* edge = new Edge(cP,Direction::NORTH);
                edgesList.append(edge);
                cE = edge;
            }
            else // pW is land
            {
                cE->length++;
                cP = pW;
            }
            break;
        }
        }
    }

    if( edgesList.isEmpty() )
        qCritical() << "[EE] edgesList is empty.";

    //convert edges list into a set of control points
    Edge* Pedge = edgesList.last();
    foreach(Edge* Cedge, edgesList)
    {
        qDebug() << "P" << Pedge->start << Pedge->dir();
        qDebug() << "C" << Cedge->start << Cedge->dir();
        QPoint one = QPoint(-1,-1);
        QPoint two = QPoint(-1,-1);
        QPoint three = QPoint(-1,-1);

        if( Pedge->direction == Direction::WEST && Cedge->direction == Direction::NORTH )
        {
            one = Cedge->start;
            if( Pedge->length > 2 )
                two = QPoint(Cedge->start.x()+1,Cedge->start.y());
            if( Cedge->length > 2 )
                three = QPoint(Cedge->start.x(),Cedge->start.y()-1);
        }
        else if( Pedge->direction == Direction::WEST && Cedge->direction == Direction::SOUTH )
        {
            one = Cedge->start;
            if( Pedge->length > 2 )
                two = QPoint(Cedge->start.x()+1,Cedge->start.y());
            if( Cedge->length > 2 )
                three = QPoint(Cedge->start.x()-1,Cedge->start.y()+1);
        }
        else if( Pedge->direction == Direction::NORTH && Cedge->direction == Direction::EAST )
        {
            one = Cedge->start;
            if( Pedge->length > 2 )
                two = QPoint(Cedge->start.x(),Cedge->start.y()+1);
            if( Cedge->length > 2 )
                three = QPoint(Cedge->start.x()+1,Cedge->start.y());
        }
        else if( Pedge->direction == Direction::NORTH && Cedge->direction == Direction::WEST )
        {
            one = Cedge->start;
            if( Pedge->length > 2 )
                two = QPoint(Cedge->start.x(),Cedge->start.y()+1);
            if( Cedge->length > 2 )
                three = QPoint(Cedge->start.x()-1,Cedge->start.y()-1);
        }
        else if( Pedge->direction == Direction::SOUTH && Cedge->direction == Direction::EAST )
        {
            one = Cedge->start;
            if( Pedge->length > 2 )
                two = QPoint(Cedge->start.x(),Cedge->start.y()-1);
            if( Cedge->length > 2 )
                three = QPoint(Cedge->start.x()+1,Cedge->start.y()+1);
        }
        else if( Pedge->direction == Direction::SOUTH && Cedge->direction == Direction::WEST )
        {
            one = Cedge->start;
            if( Pedge->length > 2 )
                two = QPoint(Cedge->start.x(),Cedge->start.y()-1);
            if( Cedge->length > 2 )
                three = QPoint(Cedge->start.x()-1,Cedge->start.y());
        }
        else if( Pedge->direction == Direction::EAST && Cedge->direction == Direction::NORTH )
        {
            one = Cedge->start;
            if( Pedge->length > 2 )
                two = QPoint(Cedge->start.x()-1,Cedge->start.y());
            if( Cedge->length > 2 )
                three = QPoint(Cedge->start.x()+1,Cedge->start.y()-1);
        }
        else if( Pedge->direction == Direction::EAST && Cedge->direction == Direction::SOUTH )
        {
            one = Cedge->start;
            if( Pedge->length > 2 )
                two = QPoint(Cedge->start.x()-1,Cedge->start.y());
            if( Cedge->length > 2 )
                three = QPoint(Cedge->start.x(),Cedge->start.y()+1);
        }

        if( two.x() > 0 || two.y() > 0)
            controlPoints.append(two);
        controlPoints.append(one);
        if( three.x() > 0 || three.y() > 0)
            controlPoints.append(three);
        Pedge = Cedge;
    }

    // close the contour
    controlPoints.append(controlPoints.first());

    // amplifier/resize
    for( int i = 0; i < controlPoints.size() ; i++ )
    {
        qDebug() << controlPoints[i];
        controlPoints[i] = QPoint(2*controlPoints[i].x(),2*controlPoints[i].y());
    }

    // random displacment
    /// todo




    // let's try to draw the contour
    QPixmap pixmap (300,300);
    pixmap.fill(Qt::white);

    QPainter painter(&pixmap);
    painter.setPen(Qt::black);
    painter.drawPolyline(controlPoints);
    pixmap.save("test.png");



//    return app.exec();

    edgesList.clear();
    delete qimage;
    delete image;
    delete object;
}
